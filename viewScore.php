<?php
// Database connection information
$servername = '127.0.0.1:3306';
$username = 'troubadour';
$password = 'd0nj0n$brosset?';
$dbname = 'projetDevDur2023';

try {
    // Connect to the database using PDO
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);

    // Set PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Prepare and execute the SELECT query with descending order and limit to 10 rows
    $stmt = $conn->prepare("SELECT name, id, score FROM scoreboard ORDER BY score DESC LIMIT 10");
    $stmt->execute();

    // Display the results
    echo "<table>";
    echo "<tr><th>Pseudo</th><th>ID</th><th>Score</th></tr>";
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        echo "<tr>";
        echo "<td>" . $row['name'] . "</td>";
        echo "<td>" . $row['id'] . "</td>";
        echo "<td>" . $row['score'] . "</td>";
        echo "</tr>";
    }
    echo "</table>";
} catch (PDOException $e) {
    echo 'An error occurred while retrieving the data: ' . $e->getMessage();
}

// Close the database connection
$conn = null;
