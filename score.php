<?php
$servername = '127.0.0.1:3306';
$username = 'xxx';
$password = 'xxx';
$dbname = 'xxx';
if (is_numeric($_POST['score'])) {
    $score = $_POST['score'];
    $name = htmlspecialchars($_POST['name']);
} else {
    $score = 1;
    $name = htmlspecialchars($_POST['name']);
}
try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("INSERT INTO scoreboard (score, name) VALUES (:score, :name)");
    $stmt->bindParam(':score', $score);
    $stmt->bindParam(':name', $name);
    $stmt->execute();
    echo 'Score has been sent to database !';
} catch (PDOException $e) {
    echo "Couldn't sent score in database : " . $e->getMessage();
}
$conn = null;
