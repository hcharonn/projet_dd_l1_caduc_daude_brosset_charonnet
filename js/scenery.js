// ---- Scenery ---- //
// Setting canvas and context
const canvasSc = document.getElementById("background"); // Get the background canvas element
const contextBG = canvas.background.getContext("2d"); // Get the 2D context for the background canvas
const canvasScL = document.getElementById("lights"); // Get the lights canvas element
const contextLi = canvas.lights.getContext("2d"); // Get the 2D context for the lights canvas
// Scenery Sources
const scenery = [ // Array of scenery images
    {
        src: "../scenery/00_sky_sc.svg", // Sky image source
    },
    {
        src: "../scenery/01_buildings_sc.svg", // Buildings image source
    },
    {
        src: "../scenery/02_street_sc.svg", // Street image source
    },
    {
        src: "../scenery/03_lights_sc.svg", // Lights image source
    },
    {
        src: "../wastes/circle_white.svg", // Lights image source
    },
    /*{
        src: "../wastes/circle_green.svg", // Lights image source
    },
    {
        src: "../wastes/circle_red.svg", // Lights image source
    },*/
];
// Loading backgrounds and lights
scenery.forEach(function (item) { // Loop through each scenery item
    let image = new Image(); // Create a new image element
    image.src = item.src; // Set the image source
    item.image = image; // Store the image in the item object
    image.onload = checkAllImagesLoaded; // Call checkAllImagesLoaded when the image is loaded
});
// Functions //
let imagesLoaded = 0; // Counter for loaded images
function checkAllImagesLoaded() { // Function to check if all images are loaded
    imagesLoaded++; // Increment the counter
    if (imagesLoaded === scenery.length) { // If all images are loaded call the drawScenery function
        drawScenery();
    }
}
function drawScenery() { // Function to draw the scenery
    const svgWidth = window.innerWidth; // Get the window width
    const svgHeight = svgWidth / scenery[0].image.width * scenery[0].image.height; // Calculate the scaled height based on the aspect ratio of the first image (crop top height to always fit width)

    canvasSc.style.position = "fixed"; // Set the position of the background canvas
    canvasSc.style.left = "0"; // Set the left position of the background canvas
    canvasSc.style.bottom = "0"; // Set the bottom position of the background canvas

    contextBG.drawImage(scenery[0].image, 0, canvasSc.height - svgHeight, svgWidth, svgHeight); // Draw the sky image on the background canvas
    contextBG.drawImage(scenery[1].image, 0, canvasSc.height - svgHeight, svgWidth, svgHeight); // Draw the buildings image on the background canvas
    contextBG.drawImage(scenery[2].image, 0, canvasSc.height - svgHeight, svgWidth, svgHeight); // Draw the street image on the background canvas
    contextLi.drawImage(scenery[3].image, 0, canvasSc.height - svgHeight, svgWidth, svgHeight); // Draw the lights image on the lights canvas
}

