/* init.js */
// Initialization & global utilities

/* jshint -W104 */
/* jshint -W119 */

/* --- Global functions --- */

// Linear interpolation
function lerp(a, b, t) {
    return (1 - t) * a + t * b;
}

// Gets a column from a
const getColumn = (anArray, columnNumber) =>
    anArray.map(row => row[columnNumber]);

/* --- Canvas ---*/

const canvas = {
    background: document.getElementById('background'),
    foreground: document.getElementById('foreground'),
    foregroundWst: document.getElementById('foregroundWst'),
    player: document.getElementById('player'),
    lights: document.getElementById('lights'),
    ui: document.getElementById('ui'),
    uiWst: document.getElementById('uiWst')
};

Object.values(canvas).forEach(item => {
    item.width = window.innerWidth;
    item.height = window.innerHeight;
});

/* Alternative solution:
document.querySelectorAll('#'+['background', 'foreground', 'player', 'ui'].join(', #')).forEach(el => Object.assign(el, {
  width: window.innerWidth,
  height: window.innerHeight
}))*/

/* --- Delta time handling --- */
// Used in gameplay logic  calculations to take frame time in account (ex: to
// prevent physics from changing based on frame rate)
var lastUpdate = performance.now(); // Initialisation
var dt = 0; // delta represents the time in ms since the last refresh
var dtScale = 1; // scalar on which to base gameplay logic calculations for variable time frame 
