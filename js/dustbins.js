// --- Dustbins generation --- //
// Dustbins list, with coords
const dustbins = [];
const dustbinTypes = ['yellow', 'blue', 'compost', 'glass'];
const nbDustbins = 4;
// Dustbin class
class Dustbin {
    constructor(x, y, width, height, binType) {
        // CanvasContext
        this.context = canvas.foreground.getContext('2d');

        // this.position
        this.position = {
            x,
            y
        };
        this.width = width;
        this.height = height;
        this.binType = binType; // Dustbin type (yellow, blue, compost, glass)
        this.binSprite = null; // Played sprite
        this.binSprites = null; // Gonna contains all the matching type sprites        
        this.isPlayerNear = false; // This will be activated by player class to avoid massive amount of request
        // Animation interval ID
        this.animationInterval = null;
        this.currentSpriteIndex = 0;
    }
    // Update when player is near (and has matching waste / or not ?)
    playerNear() {
        if (!this.isPlayerNear) {
            this.isPlayerNear = true;
            if (this.binType !== "glass") {
                this.startAnimation();
            } else {
                this.shakingBin();
            }
        }
    }
    startAnimation() { // Start the animation        
        this.stopAnimation()// Clear any existing animation interval
        this.animationDirection = 0; // Set the initial animation direction to increment
        this.animationInterval = setInterval(() => { // Start a new animation interval
            this.updateSpriteIndex();
        }, 100);
    }
    stopAnimation() { // Stop the animation        
        if (this.animationInterval) { // Clear the animation interval if it exists
            clearInterval(this.animationInterval);
            this.animationInterval = null;
        }
    }
    updateSpriteIndex() { // Update the sprite index for bins animations
        if (this.animationDirection === 0) {
            if (this.currentSpriteIndex < 5) { // Increment the index until it reaches 5
                this.currentSpriteIndex++;
            } else {
                this.animationDirection = 1; // When it reaches 5, change the animation direction to decrement
            }
        } else if (this.animationDirection === 1) {
            if (this.currentSpriteIndex > 0) { // Decrement the index until it reaches 0
                this.currentSpriteIndex--;
            } else {
                // When it reaches 0, stop the animation
                this.stopAnimation();
            }
        }
        this.binSprite = this.binSprites[this.currentSpriteIndex];
        this.context.clearRect(this.position.x - 17, this.position.y - 35, 100, 100);
        this.context.beginPath();
        this.context.drawImage(this.binSprite, this.position.x - 17, this.position.y - 35, 100, 100);
    }
    shakingBin() {
        let shakeOffset = 3; // Shaking intensity
        let initialX = this.position.x - 17; // Initial position to get back to those at the end of the animation
        let initialY = this.position.y - 35; //
        let animationDuration = 300; // Duration of the animation in milliseconds
        let startTime = null;

        let animate = (timestamp) => {
            if (!startTime) startTime = timestamp;
            let elapsed = timestamp - startTime;

            if (elapsed < animationDuration) {
                let progress = elapsed / animationDuration;
                let shakeX = Math.sin(progress * 2 * Math.PI) * shakeOffset;
                let shakeY = Math.cos(progress * 2 * Math.PI) * shakeOffset;

                this.context.clearRect(this.position.x - 17, this.position.y - 35, 100, 100);
                this.context.beginPath();
                this.context.drawImage(
                    this.binSprite,
                    initialX + shakeX,
                    initialY + shakeY,
                    100,
                    100
                );

                requestAnimationFrame(animate);
            } else {
                // Animation finished, reset to initial position
                this.context.clearRect(this.position.x - 17, this.position.y - 35, 100, 100);
                this.context.beginPath();
                this.context.drawImage(
                    this.binSprite,
                    initialX,
                    initialY,
                    100,
                    100
                );
            }
        };
        requestAnimationFrame(animate); // Reset when animation finished
    }
    //drawing
    draw() {
        this.context.clearRect(this.position.x - 17, this.position.y - 35, 100, 100);
        this.context.beginPath();

        if (!this.binSprites) {
            switch (this.binType) { // select the future drawing of binSprite depending on its parameter/value for this instance
                case 'yellow':
                    this.binSprites = dustbinYSprites; // Loading matching list of sprites
                    break;
                case 'blue':
                    this.binSprites = dustbinBSprites; // ""
                    break;
                case 'compost':
                    this.binSprites = compostSprites; // ""
                    break;
                case 'glass':
                    this.binSprites = dustbinGSprites; // ""
                    break;
                default:
                    console.log('Error : No binType passed into generateDustbin or in constructor function.')
                    break;
            }
            this.binSprite = this.binSprites[0]; // Set up the closed bin sprite
            this.context.drawImage(this.binSprite, this.position.x - 17, this.position.y - 35, 100, 100); // Draw at foreground canvas bottom
        }
    }
}
// Dustbin generation
function generateDustbin(binType) {

    let x = 0.20 * canvas.foreground.width * (dustbins.length + 1);
    let y = 1 * canvas.foreground.height - 100;

    dustbins.push(new Dustbin(x, y, 60, 60, binType)); // create a dustbin instance

    dustbins[dustbins.length - 1].draw();
}
// Dustbins Sprites handling
const dustbinYSprites = [] //yellowDustBin(plastics&cardboard)
const compostSprites = [] //compost(biodegradable)
const dustbinBSprites = [] //blue(household_waste)
const dustbinGSprites = [] //green/glass
// Load all sprites into their corresponding list
function callABin() {
    const randomIndex = Math.floor(Math.random() * dustbinTypes.length); // Choose between fourth type of bin
    let binType = dustbinTypes[randomIndex];
    binType = dustbinTypes.splice(randomIndex, 1)[0]; // Splice random index so there is always four different types of bins on playground
    switch (binType) {  // Pass randomly selected binType to load its matching sprites
        case 'yellow':
            loadBinsSprites(dustbinYSprites, 'yellow', 6); // (Matching bin type list to fill , bin type , how much files for this type)
            break;
        case 'blue':
            loadBinsSprites(dustbinBSprites, 'blue', 6); // ""
            break;
        case 'compost':
            loadBinsSprites(compostSprites, 'compost', 6); // ""
            break;
        case 'glass':
            loadBinsSprites(dustbinGSprites, 'glass', 1); // ""
            break;
        default:
            break;
    }
}
// Loads all sprites for a bin type
function loadBinsSprites(dustbinSprites, binType, spritesCount) { // (Matching bin type list to fill , bin type , how much files for this type)
    let loadedCount = 0;

    for (let i = 0; i < spritesCount; i++) { // This loop is loading all sprites files into corresponding list
        const sp = new Image();
        sp.src = '../sprites/dustbins/' + binType + 'Dustbin_' + i + '.svg';
        dustbinSprites.push(sp);
        sp.onload = function () {
            loadedCount++;
            if (loadedCount === spritesCount) { // If all sprites need for a bin type are loaded then generate bin
                generateDustbin(binType);
            }
        };
    }
}

