/* main.js */
// Main file. Calls take place here.

/* jshint -W104 */
/* jshint -W119 */
// User entering is surname 
const pseudo = prompt("Veuillez entrer votre pseudo :");

let debug = document.getElementById('debug');

const duration = 17; // ~17ms -> frame time for 60fps. Arbitrary 'golden value' for variable time step scaling.

/**
  * Main loop. One frame corresponds to one call of this function.
  */
function tickUpdate() {
  // requestAnimationFrame() requests a callback to the browser before the
  // next redraw.
  // In this case, we call tickUpdate again. This is what allows us to have a
  // main loop with one iter per frame.
  requestAnimationFrame(tickUpdate);
  // TODO: I don't know where to place requestAnimationFrame() since behaviour
  // doesn't seem to change regardless of order

  // Drawing & moving player
  player.update();

  // Updating delta time
  let now = performance.now();
  dt = now - lastUpdate;
  lastUpdate = now;

  dtScale = dt / duration;

  debug.textContent = "dt: " + dt + " ; player.is_on_floor: " + player.is_on_floor;
}


function sendScore(score, pseudo) {
  if (pseudo !== null) {
    fetch('../score.php', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: 'score=' + score + '&name=' + encodeURIComponent(pseudo),
    })
      .then(response => {
        console.log('Fetch is a great success !');
      })
      .catch(error => {
        console.error('Fetch is a great failure :', error);
      });
  }
}


function generateTurn() {
  // Wastes skins loading
  callWasteSkinsLoading()
  // Dustbins generation
  for (let i = 0; i <= nbDustbins; i++) {
    callABin();
  }
  // Procedural generation of platforms
  generatePlatforms();
  //Procedural generation of wastes every x secs (TMP)
  startWasteGeneration();
}

const player = new Player();
generateTurn();
tickUpdate();
console.log("Ready!");
function gameOver() {
  // this will be call at the end of a game.
  sendScore(player.score, pseudo);
  alert('Game over ' + pseudo + ' votre score est de ' + score + ' soit ' + score / 8 + 'déchets ramasser. Ce dernier a été envoyer à notre base de données.');
}

function startTimer() {

  setTimeout(gameOver, 2 * 60 * 1000); // 2minutes timer 
}
// Start a game
startTimer();
// const foo = new Platform();
// const bar = new Platform();
// foo.position.x = 100;
// foo.position.y = 100;
// bar.position.x = 150;
// bar.position.y = 150;
// foo.width = 100;
// foo.height = 100;
// bar.width = 100;
// bar.height = 100;
// foo.draw();
// bar.draw();
