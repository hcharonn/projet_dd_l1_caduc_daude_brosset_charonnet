/* jshint esversion: 6*/
// ----- Plaforms ----- //
const platforms = []; // Platforms placement (x , y, width, height)
function randomPlatformNumber() { //This function will generate a random number from an array containing the possible plateform numbers, 5, 7, and 9.                                     
    let outcomes = [20, 25, 30]; //It returns the randomly chosen number.
    let nb = Math.floor(Math.random() * outcomes.length);
    return outcomes[nb];
}
function randomPlatformSize() { //This function returns a random size of platform from the possible list of 25, 50, 100, and 200.    
    let outcomes = [25, 50, 100, 200];
    let nb = Math.floor(Math.random() * outcomes.length);
    return outcomes[nb];
}
function generatePlatforms() {
    let nb = randomPlatformNumber(); // Select random number of platforms in a predetermined array
    for (let i = 0; i < nb; i++) {
        // Random position
        let x = (Math.random() * Math.random()) * canvas.foreground.width;
        let y = (Math.random() * Math.random()) * canvas.foreground.height;
        // Select random platform size in a predetermined array
        let width = randomPlatformSize();
        let newPlatform = new Platform(x, y, width); // create a new instance of platform
        let collision = checkPlatformCollision(newPlatform); // check if new platform instance  is in collision with another
        let topAndBottomcheck = checkTopAndBottom(newPlatform); // check if new platform instance is not too high or on players and dustbins spawn
        if (collision) {
            i--; // decrement i in order to recreate a platform instance if there is collision
        } else if (topAndBottomcheck) {
            i--; // decrement i if the platform is too high or too low
        } else { // draw the platform if collision and topAndBottom check are false :)))
            platforms.push(newPlatform);
        }
    }
    // draw all platforms
    platforms.forEach((pf) => {
        pf.draw();
    });
}
// check collisions function for platforms (so they can't have sex between each other)
function checkPlatformCollision(newPlatform) {
    for (let i = 0; i < platforms.length; i++) {
        let existingPlatform = platforms[i];
        if ( // check collision with others platforms
            newPlatform.position.x + newPlatform.width + 20 >= existingPlatform.position.x && // minimum width 20 pixel between platforms
            newPlatform.position.x - 20 <= existingPlatform.position.x + existingPlatform.width && // 
            newPlatform.position.y + newPlatform.height + 80 >= existingPlatform.position.y && // minimum height of 80 pixel between platforms
            newPlatform.position.y - 80 <= existingPlatform.position.y + existingPlatform.height// 

        ) {
            return true; // if there is a collision with another platform
        }
    }
    return false; // no collision, all good saul !
}
// check for platforms so they don't spawn on top limit of the screen or on player / dustbins
function checkTopAndBottom(item) {
    if (
        item.position.y >= canvas.foreground.height - 160 || // ensure platforms to spawn higher than player and dustbins
        item.position.y <= 110 // ensure platforms not to spawn on top limit of canvas (not always working and dunno why)
    ) {
        return true; // if a platform is too high or too low :))
    }
    return false; // this platform stays alive YESSAI!
}
//A platform of variable width and height. Drawn on turn generation on "foreground" layer. //
class Platform {
    constructor(x, y, width = 100, height = 10) {
        this.context = canvas.foreground.getContext('2d'); // CanvasContext     
        this.position = { // this.position
            x,
            y
        };
        this.width = width;
        this.height = height;
        this.texture = new Image();
        this.texture.src = "../scenery/bricks.png";
    }
    draw() { // classic draw function :) (i'm so depressed)
        this.context.clearRect(this.position.x, this.position.y, this.width, this.height);

        this.context.fillStyle = 'black'; // Just to add some perspectives
        this.context.beginPath(); // ""
        this.context.fillRect(this.position.x, this.position.y, this.width + 2, this.height + 2); // ""

        let self = this; // forced to use this variable so this goes into texturing (stupid js :))
        function texturing() {
            let pattern = self.context.createPattern(self.texture, 'repeat');
            self.context.fillStyle = pattern;
            self.context.beginPath();
            self.context.fillRect(self.position.x, self.position.y, self.width, self.height);
        }
        this.texture.onload = function () { // call texturing only if bricks are loaded :))))))
            texturing();
        };
    }
}

