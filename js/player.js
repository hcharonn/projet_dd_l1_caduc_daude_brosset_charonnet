/* player.js */

/* jshint -W104 */
/* jshint -W119 */

/**
  * Player. Drawn every frame on "player" layer.
 */
class Player {
    constructor() {
        // CanvasContext
        this.context = canvas.player.getContext('2d');
        // Player position; (0,0) -> top left corner of the canvas
        this.position = {
            x: 0.5 * canvas.foreground.width, // spawn is center of our canvas
            y: canvas.foreground.height - 100 // spawn is bottom of our canvas // 80 is player height
        };

        // Used for collision and debug graphics
        this.width = 35; // 20
        this.height = 80; // 80

        /* Physical properties */
        /* Do not reassign these. Consider these immutable. */

        // Horizontal speed
        this.speed = 5;
        // Force applied upon jumping
        this.jump_speed = -15;
        // Downwards force applied every tick while in the air
        this.gravity = 0.4;
        // Horizontal acceleration
        this.acceleration = 0.1;
        // Horizontal friction
        this.friction = 0.1;
        this.inventory = [[], [], [],];
        /* Movement variables */

        // Horizontal movement direction (-1: left, 0: idle, 1: right)
        this.dir = 0;
        // Velocity 2D vector
        this.velocity = {
            x: 0,
            y: 0
        };
        this.is_on_floor = false;

        // Score
        this.score = 0;

        /* --- Input handling ---- */

        // TODO-POST: For fun, try to implement:
        //              Input buffer
        //              Press, hold, release behaviour
        //              Finite State Machine

        // Each property holds an arrow function which calls a corresponding
        // controls method. Property names correspond DIRECTLY to `event.code`.
        // Arrow funcs are used because attempting to store a reference to the
        // method leads to scope issues and prevents use of arguments.
        this.keybinds = {
            ArrowLeft: () => this.move(-1),
            ArrowRight: () => this.move(1),
            Space: () => this.jump(),
            Enter: () => this.start(),
        };

        // Array list containing currently held keys
        this.pressed = [];

        // When a key is held, its code is added to the list of held keys.
        window.addEventListener(
            "keydown",
            (event) => {
                if (this.keybinds[event.code] !== undefined) {
                    if (!this.pressed.includes(event.code)) {
                        this.pressed.push(event.code);
                    }
                }
            }
        );

        // When a key is released, its code is removed from the list of held
        // keys.
        window.addEventListener(
            "keyup",
            (event) => {
                if (this.keybinds[event.code] !== undefined) {
                    if (this.pressed.includes(event.code)) {
                        this.pressed.splice(this.pressed.findIndex(element => element === event.code), 1);
                    }
                }
            }
        );


    }

    // Called every tick. Calls corresponding method for every held key.
    handleInput() {
        this.pressed.forEach((key) => {
            this.keybinds[key]();
        });
    }

    /*setKeybinds(action, key) {
        // TODO: redo for new input system
    }*/

    // Checks if player is colliding with floor. To check if you can jump or
    // change physics based on state
    isOnFloor() {
        // TODO: check if colliding downwards
        return;
    }

    /**
     * Update function. Called every tickUpdate() call.
     * Applies current velocity to position, then calculates velocity for the
     * next update with calculateVelocity().
     * Currently, also draws player as a red square for debug. This will change
     * when sprite integration is implemented.
     */

    update() {
        this.calculateVelocity();
        this.position.x += this.velocity.x;
        this.position.y += this.velocity.y;
        this.handleInput();

        this.nextX = this.position.x + this.velocity.x; // positions anticipations
        this.nextY = this.position.y + this.velocity.y; // useful for checkCollision() (used to check collision with trash and bins)
        this.isNearABin();
        this.pickUpWaste();
        // Complety cleans canvas.
        this.context.clearRect(0, 0, canvas.player.width, canvas.player.height);
        this.context.beginPath(); // Commence le tracé
        //this.context.closePath();
        //this.context.fillStyle = 'blue';
        //this.context.fillRect(this.position.x, this.position.y, this.width, this.height);


        // Sprites draw //
        callPlayerSprites();
        this.context.drawImage(sprite, this.position.x - 22, this.position.y, 80, 80); //(skin, draw x pos , draw y pos , width , height) // -22 on x spawn to get sprite in the middle of the player context

    }

    /**
     * Calculates velocity for next update.
    */
    calculateVelocity() {
        // Check if player going out of canvas (top & bottom)
        if (this.position.y + this.velocity.y < 0) {
            this.velocity.y = -this.position.y; // player cannot go out top side
            this.is_on_floor = false;
        }
        if (this.position.y + this.height + this.velocity.y <= canvas.player.height - 20) {
            this.velocity.y = this.velocity.y + (this.gravity * dtScale);
            this.is_on_floor = false;
        } else {
            this.is_on_floor = true;
            this.velocity.y = 0; //If on floor, no vertical velocity.

        }
        platforms.forEach((item, i) => {
            if (/*
                this.position.y + this.height <= item.position.y &&
                this.position.y + this.height + this.velocity.y >= item.position.y &&
                this.position.x + this.width >= item.position.x &&
                this.position.x >= item.position.x &&
                this.position.x <= item.position.x + item.width
                */
                this.checkPlatform(item)
            ) {
                this.velocity.y = 0;
                this.is_on_floor = true;
            } else {

            }
        });

        if (this.dir != 0) { // If NOT immobile,
            // Linear transition of velocity towards speed constant.
            this.velocity.x = lerp(this.velocity.x, this.dir * this.speed, this.acceleration);
        } else { // If immobile,
            // Linear transition of velocity towards 0.
            this.velocity.x = lerp(this.velocity.x, 0, this.friction);
        }

        this.dir = 0;

        // Check if player going out of canvas (left & right)
        if (this.position.x + this.velocity.x < 0) {
            this.velocity.x = -this.position.x; // player cannot go out left side
        } else if (this.position.x + this.velocity.x + this.width > canvas.foreground.width) {
            this.velocity.x = canvas.foreground.width - this.position.x - this.width; // player cannot go out right side
        }

    }
    // Defines movement direction.
    // Exists mainly to be redefined later.
    move(dir) {
        //console.log("Moving! " + dir + " " + this.position);
        this.dir = dir;
    }
    jump() {
        //console.log("wahoo! " + this.velocity.y);
        if (this.is_on_floor) {
            this.velocity.y += this.jump_speed;
        }
    }
    checkPlatform(item) {
        if (
            this.position.y + this.height <= item.position.y &&
            this.position.y + this.height + this.velocity.y >= item.position.y &&
            this.position.x + this.width >= item.position.x &&
            this.position.x >= item.position.x - item.width &&
            this.position.x <= item.position.x + item.width
        ) {
            return true;
        } else {
            return false;
        }
    }
    checkCollision(item) {      // this.nextY = this.position.y + this.velocity.y; this.nextX = this.position.x + this.velocity.x;
        if (                                                // check collision for those directions:
            this.nextY + this.height >= item.position.y && // from top (player is going bottom direction)
            this.nextY <= item.position.y + item.height && // from bottom (player is going top direction)
            this.nextX + this.width >= item.position.x && // from left ((player is going right direction))
            this.nextX <= item.position.x + item.width // from right (player is going left direction)
        ) {
            return true; // collision
        } else {
            return false; // no collision
        }
    }
    isNearABin() {
        // Is player near a bin:
        dustbins.forEach((bin, i) => {
            if (this.checkCollision(bin)) { //
                dustbins[i].playerNear();
                dustbins[i].isPlayerNear = true;
                this.putWasteInBin(dustbins[i]);
            } else if (dustbins[i].isPlayerNear) { // isPlayerNear allow to launch item's function only once till player is on the item
                dustbins[i].isPlayerNear = false;
            }
        });
    }
    pickUpWaste() {
        wastesOnMap.forEach((waste, i) => {
            if (!this.inventory[0][0] || !this.inventory[1][0] || !this.inventory[2][0]) {
                if (this.checkCollision(waste)) {
                    //this.inventory[i][0].push(waste);
                    wastesOnMap.splice(i, 1);
                    //waste.context.fillRect(0, 0, 300, 300);
                    waste.context.clearRect(waste.position.x, waste.position.y, waste.width, waste.height);
                    waste.context.beginPath(); // Commence le tracé
                    if (!this.inventory[0][0]) {
                        this.inventory[0][0] = waste;
                        contextUIWst.drawImage(waste.wasteSkin, 10, canvaUI.height - 135, 70, 70);
                    }
                    else if (!this.inventory[1][0]) {
                        this.inventory[1][0] = waste;
                        contextUIWst.drawImage(waste.wasteSkin, 110, canvaUI.height - 135, 70, 70);
                    }
                    else if (!this.inventory[2][0]) {
                        this.inventory[2][0] = waste;
                        contextUIWst.drawImage(waste.wasteSkin, 210, canvaUI.height - 135, 70, 70);

                    }
                }

            } else if (wastesOnMap[i].isPlayerNear) { // isPlayerNear allow to launch item's function only once till player is on the item
                wastesOnMap[i].isPlayerNear = false;
            }
        });
    }
    putWasteInBin(dustbin) {

        if (this.inventory[0][0] || this.inventory[1][0] || this.inventory[2][0]) {
            //this.inventory.forEach((waste, i) => {
            if (this.inventory[0][0]) {
                if (dustbin.binType == this.inventory[0][0].wasteType) {
                    contextUIWst.clearRect(10, canvaUI.height - 135, 70, 70);
                    this.inventory[0][0] = "";
                    this.score += 8;
                }
            }
            if (this.inventory[1][0]) {
                if (dustbin.binType == this.inventory[1][0].wasteType) {
                    contextUIWst.clearRect(110, canvaUI.height - 135, 70, 70);
                    this.inventory[1][0] = "";
                    this.score += 8;
                }
            }
            if (this.inventory[2][0]) {
                if (dustbin.binType == this.inventory[2][0].wasteType) {
                    contextUIWst.clearRect(210, canvaUI.height - 135, 70, 70);
                    this.inventory[2][0] = "";
                    this.score += 8;
                }
            }

        }
    }
}
