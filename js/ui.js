const contextUI = canvas.ui.getContext("2d");
const canvaUI = document.getElementById("ui")
const contextUIWst = canvas.uiWst.getContext("2d");
const canvaUIWst = document.getElementById("uiWst")
const ui = [
    {
        src: "../wastes/circle_white.svg", // Lights image source
    },
    /*{
        src: "../wastes/circle_green.svg", // Lights image source
    },
    {
        src: "../wastes/circle_red.svg", // Lights image source
    },*/
]
ui.forEach(function (item) { // Loop through each scenery item
    let image = new Image(); // Create a new image element
    image.src = item.src; // Set the image source
    item.image = image; // Store the image in the item object
    image.onload = checkAllImagesLoaded; // Call checkAllImagesLoaded when the image is loaded
});
let imagesLoadedUI = 0; // Counter for loaded images
function checkAllImagesLoaded() { // Function to check if all images are loaded
    imagesLoadedUI++; // Increment the counter
    if (imagesLoadedUI === ui.length) { // If all images are loaded call the drawScenery function
        drawUI();
    }
}
function drawUI() {
    contextUI.drawImage(ui[0].image, -80, canvaUI.height-200, 250,250);
    contextUI.drawImage(ui[0].image, 20,canvaUI.height-200, 250,250);
    contextUI.drawImage(ui[0].image, 120, canvaUI.height-200, 250,250);
}
