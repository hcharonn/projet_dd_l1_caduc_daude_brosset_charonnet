/* jshint esversion: 6 */

// ----- WASTES ----- // Don't get wasted pick up some waste instead ...
const wasteTypes = ['yellow', 'blue', 'compost', 'glass']; // Type of bin the waste goes (yellow, blue, compost, glass)
const wastesOnMap = []; // List of wastes on map so they don't spawn on each other



//Randomization function:
function randomWastesNb() { // This function returns a random number between 4 and 8 to represent the number of waste.
    let outcomes = [1, 1, 1]; // How much wastes will be spawned ? Let's get ready to ruuuummmmmmmmmmble
    let nb = Math.floor(Math.random() * outcomes.length); //Math.floor returns the largest integer that is less than or equal to a number x
    return outcomes[nb];
}
function randomWasteType() { // Randomly select a waste's type
    let nb = Math.floor(Math.random() * wasteTypes.length); // Select random type of wastes in a predetermined array
    return wasteTypes[nb];
}
// WASTE CLASS
class Waste { // This class represents a waste
    constructor(x, y, type) { // It stores the x and y coordinates of this waste.
        this.context = canvas.foregroundWst.getContext('2d');
        this.position = {
            x,
            y
        };
        this.width = 80;
        this.height = 80;
        this.wasteType = type; // In which type of bin the waste goes (yellow, blue, compost, glass)
        this.isPlayerNear = false;// This will be activated by player class to avoid massive amount of request
        this.wasteSkin;// Futur appearence of this waste
    }
    draw() {
        this.context.clearRect(this.position.x, this.position.y, this.height, this.width);
        this.context.beginPath();
        let rI; // random Index
        //console.log(this.wasteType) WORK
        switch (this.wasteType) { // select the future drawing of binSprite depending on its parameter/value for this instance
            case 'yellow':
                rI = Math.floor(Math.random() * yellowWasteSkins.length);
                this.wasteSkin = yellowWasteSkins[rI]; // randomly select a skin in the corresponding list of skins
                break;
            case 'blue':
                rI = Math.floor(Math.random() * blueWasteSkins.length);
                this.wasteSkin = blueWasteSkins[rI]; // ""
                break;
            case 'compost':
                rI = Math.floor(Math.random() * compostWasteSkins.length);
                this.wasteSkin = compostWasteSkins[rI]; // ""
                break;
            case 'glass':
                rI = Math.floor(Math.random() * glassWasteSkins.length);
                this.wasteSkin = glassWasteSkins[rI]; // ""
                break;
            default:
                console.log('Error : No waste\'s type.');
                break;

        } // Set up the closed bin sprite
        this.context.drawImage(this.wasteSkin, this.position.x, this.position.y, this.width, this.height); // Draw on foreground canvas

    }
}
function generateWaste() {
    let nb = randomWastesNb();
    const newWastes = [];
    for (let i = 0; i < nb; i++) {
        let wasteType = randomWasteType(); // randomly select a waste type
        let x = (Math.random() * Math.random()) * canvas.foreground.width; // Random position (TMP)(Note: j'aimerai bien les faire tomber du ciel partout sauf sur les poubelles)
        let y = (Math.random() * Math.random()) * canvas.foreground.height; // (Note suite: ca ne serais plus aléatoire ici et il faudrait actualisé la position)
        let newWaste = new Waste(x, y, wasteType); // create a new instance of Waste
        let collision = checkWastesCollision(newWaste); // check if new waste instance  is in collision with another
        let topAndBottomcheck = checkTopAndBottom(newWaste); // check if new waste instance is not too high or on players and dustbins spawn (Note suite: ca ne serais plus nécessaire)
        if (collision) {
            i--; // decrement i in order to recreate a waste instance if there is collision
        } else if (topAndBottomcheck) {
            i--; // decrement i if the waste is too high or too low
        } else {
            newWaste.draw();
            wastesOnMap.push(newWaste);
        }
    }
    newWastes.forEach((wst) => { // draw all wastes
        wst.draw();
    });
}
// check collisions function for wastes (so they can't have sex between each other)
function checkWastesCollision(newWaste) {
    for (let i = 0; i < wastesOnMap.length; i++) {
        let existingWaste = wastesOnMap[i];
        if ( // check collision with others wastes
            newWaste.position.x + newWaste.width + 20 >= existingWaste.position.x && // atleast 20 pixel between waste
            newWaste.position.x - 20 <= existingWaste.position.x + existingWaste.width && //
            newWaste.position.y + newWaste.height + 20 >= existingWaste.position.y && // (Note suites: Plus besoin de vérifier la ghauteur si il surviennent tous du ciel)
            newWaste.position.y - 20 <= existingWaste.position.y + existingWaste.height//

        ) {
            return true; // if there is a collision with another waste
        }
    }
    return false; // no collision, all good saul !
}
// -- Loading skins -- //
let allWstSkinsAreLoaded = false;
let wstSkinsToLoad = 8;
let wstSkinsLoaded = 0;
//Lists of waste skins for each type:
const yellowWasteSkins = [];  // Those will be choose randomly in draw function depending of the waste type
const blueWasteSkins = [];  //
const compostWasteSkins = [];  //
const glassWasteSkins = [];  //
function callWasteSkinsLoading() { // This function will be used in main to start loading wastes skins
    loadWasteSkins(yellowWasteSkins, 'yellow', 2);
    loadWasteSkins(blueWasteSkins, 'blue', 2);
    loadWasteSkins(compostWasteSkins, 'compost', 2);
    loadWasteSkins(glassWasteSkins, 'glass', 2);
}
// Loads all sprites for a waste type
function loadWasteSkins(wasteSkinsList, wasteType, skinsCount) { // (Matching bin type list to fill , bin type , how much files for this type)
    for (let i = 0; i < skinsCount; i++) { // This loop is loading all skins files into corresponding list
        const sp = new Image();
        sp.src = '../wastes/waste_' + wasteType + '_0' + i + '.svg';
        wasteSkinsList.push(sp);
        sp.onload = function () {
            wstSkinsLoaded++;
            if (wstSkinsLoaded === wstSkinsToLoad) {
                allwstSkinsAreLoaded = true;
            }
        };
    }
}
let wasteInterval;
function startWasteGeneration() {
    if (wasteInterval) {
        clearInterval(wasteInterval);
    }
    wasteInterval = setInterval(generateWaste, 5000); // 10000 millisecondes = 10 secondes
}
