//console.log('Loading sprites.js');

let animationTick = 1; // +1 on each animation looping back depending on the state
let stopAnimation = false; // if you want to stop the animation
let animationDelay = 60; // animation delay in milliseconds. lower this and animation will go faster
let lastAnimationTime = 0;
let lastVeloRight = true; // tells what was the last direction of the character (true = right // false = left)

let sprite = new Image(); // This variable/svg is used in player.js and will be drawn in update() function
const walkRSpr = [];
const walkLSpr = [];
const idleRSpr = [];
const idleLSpr = [];

let totalLoaded = 0; // +1 on each loaded sprite
const totalToLoad = 32; // Total sprites to load before launching animations

let playerState = 'idleR'; // Character state (idleR, walkR, airR, liftR, liftAR, liftIR, idleL, walkL, airL, liftL, liftAL, liftIL)

const spritesMap = new Map([
    ['walkR', walkRSpr],
    ['walkL', walkLSpr],
    ['idleR', idleRSpr],
    ['idleL', idleLSpr],
]);

// Filling lists and launching animations when all sprites are loaded
function callPlayerSprites() {
    loadSprites(walkRSpr, walkLSpr, 'walk', 10);
    loadSprites(idleRSpr, idleLSpr, 'idle', 7);
}

// Load all sprites into their corresponding list
function loadSprites(spritesListR, spritesListL, state, spritesCount) { // sprite for going right, sprite for going left, state needed, files number
    for (let i = 0; i < spritesCount; i++) {
        const sp = new Image();
        const spS = new Image();
        sp.src = '../sprites/' + state + '/' + state + '_' + i + '.svg';
        spS.src = '../sprites/' + state + '/' + state + 'S_' + i + '.svg';
        spritesListR.push(sp);
        spritesListL.push(spS);

        sp.addEventListener('load', loaded);
        spS.addEventListener('load', loaded);
    }
}

// Happens when all sprites are loaded and player exist, initialize the animation
function loaded() {
    totalLoaded++;
    if (totalLoaded === totalToLoad && player) {
        animationInit();
    }
}

// Animations initialization
function animationInit() {
    requestAnimationFrame(function (now) { // requestAnimationFrame use navigator framerate instead of our own delay value, avoiding clipping on chrome
        animation(now);
    });
}

// Animations
function animation(now) {
    if (stopAnimation) {
        // Stop animations if you set this variable to false (bool)
        return;
    }
    if (now - lastAnimationTime >= animationDelay) { // If the animation delay is inferior or equal to time this function took on its last iteration then play the animation // avoid clipping
        animationState(); // Switching playerState depending of the player velocity then get the corresponding sprite
        animationTick += 1; // increment animationTick to get an index later on
        lastAnimationTime = now; // define next iteration value of lastAnimationTime for next iteration of this recursive function
    }
    requestAnimationFrame(function (now) { // do a recursive loop of animation() till stopAnimation === true
        animation(now);
    });
}

function animationState() {
    if (player.velocity.x < -1) {
        playerState = 'walkL';
        lastVeloRight = false;
    } else if (player.velocity.x > 1) {
        playerState = 'walkR';
        lastVeloRight = true;
    } else if (lastVeloRight === true) {
        playerState = 'idleR';
    } else if (lastVeloRight === false) {
        playerState = 'idleL';
    }
    let sprites = spritesMap.get(playerState); // use sprites table that is linked to player state
    let spriteIndex = animationTick % sprites.length; // allow us to know which sprite to use in sprites table
    if (animationTick > 20) { // avoid going high value on animationTick
        animationTick = 0;
    }
    sprite = sprites[spriteIndex]; // there it is we using matching sprite :)
}