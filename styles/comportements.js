// ---- Storing variables ---- //
const burgerButton = document.querySelector('.header__burger'); // Burger
const navUl = document.querySelector('.nav__menu'); // Navigation
const wholeNav = document.querySelector('nav'); // Whole navigation (to put this in header)
const header = document.querySelector('header');
const main = document.querySelector('main');
const body = document.querySelector('body');
const mediaQuery = window.matchMedia('(min-width: 45rem)'); // Responsive/MediaQuery conditions
const guideImages = document.querySelectorAll('.guide__bins img');

// ----- Burger Menu ----- //
burgerButton.addEventListener('click', () => { // Will activate or desactivate navigation
    navUl.classList.toggle('active');
});
// ----- Navigation depending of screen width ----- /
function checkMediaQuery() { //Function to verify if screen min width >= 45rem
    if (!mediaQuery.matches) { // If window < 45rem move navigation into body between header and main, replace menu by burger
        if (header.contains(wholeNav)) {
            body.insertBefore(wholeNav, main);
            wholeNav.appendChild(navUl);
            navUl.classList.remove('active');
        }
    } else { // If window >= 45rem move navigation into header and replace burger by menu (burger display is changed to none into responsive.css)
        if (!header.contains(wholeNav)) {
            header.append(wholeNav);
            wholeNav.append(navUl);
            navUl.classList.add('active');
        } else if (header.contains(wholeNav)) {
            navUl.classList.add('active');
        }
    }
}
checkMediaQuery(); // Check at first loading
window.addEventListener('resize', checkMediaQuery); // When you are resizing your windows launch checkMediaQuery
// ----- Text element width smooth update ----- /
function updateMaxWidth() {
    let homeText = document.querySelectorAll('.home__fact1, .home__fact2, home__lore');
    let screenWidth = window.innerWidth;

    homeText.forEach((text) => {
        let maxWidthPercentage = Math.min(60, 60 - (screenWidth - 1920) * 0.01) + '%'; // Take lower value between 70 and equation.
        text.style.maxWidth = maxWidthPercentage;
    });
}
updateMaxWidth();
window.addEventListener('resize', updateMaxWidth);
// ----- Guide images width update ----- /
function updateGuideImagesWidth() {
    guideImages.forEach((img) => {
        if (mediaQuery.matches) {
            img.style.maxWidth = '33%';
        }
        if (!mediaQuery.matches) {
            img.style.maxWidth = '90%';
        }
    });
}
updateGuideImagesWidth();
window.addEventListener('resize', updateGuideImagesWidth);
// ----- Facts swapping ----- //
const factList = [
    "Le développement durable est \"un développement qui répond aux besoins du présent sans compromettre la capacité des générations futures à répondre aux leurs\" - Gro Harlem Brundtland, ancien Premier ministre Norvégien.",
    "En 2020, en France, 310 millions de tonnes de déchets ont été produites. Dont 33,7 millions de tonnes par les ménages soit plus d'un quart du total.",
    "En 2016, 44,7 millions de tonnes de déchets électroniques ont été générés. Cela équivaut à près de 4 500 tours Eiffel.",
    "Seuls 20% des déchets électroniques sont correctement recyclés. Lorsqu’ils sont mal gérés, les déchets électroniques produisent des produits chimiques dangereux qui contaminent les sol et les eaux souterraines, mettant en danger la biodiversité et les sources de nourriture et d’eau.",
    "Le plastique semble déjà omniprésent près des côtes et dans la mer. Et, alors qu’aujourd’hui il y a un ratio d’une tonne de plastique pour 5 tonnes de poisson, ce ratio devrait atteindre 1 tonne de plastique pour 3 tonnes de poisson d’ici 2025, et d’ici 2050, le plastique dans les océans dépassera le poisson.",
    "Aujourd’hui, nous produisons environ 300 millions de tonnes de déchets plastiques chaque année. C’est presque l’équivalent du poids de toute la population humaine.",
    "La moitié de tout le plastique fabriqué annuellement finit dans une poubelle au cours de sa première année de vie.",
    "Les quantités d’ordures ménagères résiduelles collectées en porte-à-porte ont connu une baisse significative de -24% entre 2005 et 2019 en France. Cela est notamment dû à la pandémie.",
    "En 2020, en Allemagne, 43 % des emballages de boissons sont encore consignés pour réemploi. Utiliser des contenants réutilisables permet d'éviter la production de déchets.",
    "Selon la Banque mondiale, un habitant produit en moyenne 0,74 kg de déchets au quotidien dans le monde, bien qu'il y ait de fortes disparités selon le niveau de vie. Plus ce dernier est élevé, plus la quantité de déchets l'est aussi.",
    "La décomposition de la proportion organique des déchets solides est à l'origine d'environ 5% des émissions mondiales de gaz à effet de serre. D'où la nécessité de réduire le volume de ses déchets.",
    "La lecture d'un document sur ordinateur pèse moins sur l'environnement que la transmission sur papier, à condition de ne consulter qu'une fois et de passer moins de trente minutes à l'écran. L'impact environnemental du papier est plus grand que celui d'une liseuse pour ceux qui lisent plus de vingt livres par an.",
    // Feel free to add facts
];
const homeFact1 = document.querySelector('.home__fact1');
const homeFact2 = document.querySelector('.home__fact2');
function getRandomFact() {
    return factList[Math.floor(Math.random() * factList.length)];
}
function changeFact(element, delay) {
    element.style.opacity = '0';
    setTimeout(() => {
        let newFact = getRandomFact();
        while (homeFact1.textContent === newFact && homeFact2.textContent === newFact) {
            newFact = getRandomFact();
        }
        element.textContent = newFact;
        element.style.opacity = '1';

        setTimeout(() => {
            changeFact(element, delay); // Recursive call to repeat this function
        }, delay - 3000); // Starting function after out fading effect
    }, 3000); // Changing fact after out fading effect for the first iteration of this recursive call
}
const delayFact1 = 14000; // Delay in milliseconds
const delayFact2 = 14000; //
setTimeout(() => { // The global setTimeout() method is used to set a timer that executes a given function or code after the specified time has elapsed.
    changeFact(homeFact1, delayFact1);
}, delayFact1);
setTimeout(() => {
    changeFact(homeFact2, delayFact2);
}, delayFact2 + 5000);