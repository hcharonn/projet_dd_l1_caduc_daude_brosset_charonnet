/* main.js */
// Main file. Calls take place here.

/* jshint -W104 */
/* jshint -W119 */

let score = 0;
let fgContext = canvas.foreground.getContext('2d');
let debug = document.getElementById('debug');

const duration = 17; // ~17ms -> frame time for 60fps. Arbitrary 'golden value' for variable time step scaling.

// Music
const audio = new Audio('./musique.mp3.mp3'); 

// Charger la musique
window.addEventListener('load', function() {
  audio.load();
});

// Arrêter la musique lorsque la page se décharge (partie terminée)
window.addEventListener('unload', function() {
  audio.pause();
  audio.currentTime = 0;
});

/**
  * Main loop. One frame corresponds to one call of this function.
  */
function tickUpdate() {
  // requestAnimationFrame() requests a callback to the browser before the
  // next redraw.
  // In this case, we call tickUpdate again. This is what allows us to have a
  // main loop with one iter per frame.
  requestAnimationFrame(tickUpdate);

  // Drawing & moving player
  player.update();

  // Updating delta time
  let now = performance.now();
  dt = now - lastUpdate;
  lastUpdate = now;

  dtScale = dt / duration;

  debug.textContent = "dt: " + dt +" ; player.is_on_floor: " + player.is_on_floor;

  // Play music
  if (audio.paused) {
    audio.play();
  }
}

function generateTurn() {
  // Wastes skins loading
  callWasteSkinsLoading()
  // Dustbins generation
  for (let i = 0; i <= nbDustbins; i++) {
    callABin();
  }
  // Procedural generation of platforms
  generatePlatforms();
  //Procedural generation of wastes every x secs (TMP)
  startWasteGeneration();
}

const player = new Player();
generateTurn();
tickUpdate();

console.log("Ready!");
