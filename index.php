<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="./styles/css/reset.css">
        <link rel="stylesheet" href="./styles/css/mobile.css">
        <link rel="stylesheet" href="./styles/css/responsive.css">
        <link rel="icon" type="image/x-icon" href="./styles/img/favicon.ico">
        <script src="./styles/comportements.js" defer></script>
        <title>GarbageDAY!.js</title>
    </head>

    <body>
        <span id="home"></span>
        <header class="header">
            <img class="header__logo" src="./styles/img/logo.svg">
            <svg class="header__burger" viewBox="0 0 24 24">
                <path d="M0 0h24v24H0z" fill="none" />
                <path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z" />
            </svg>
        </header>

        <nav class="nav">
            <ul class="nav__menu">
                <li class="menu__option"><a href="#home" class="option__link">Accueil</a></li>
                <li class="menu__option"><a href="#about" class="option__link">À propos</a></li>
            </ul>
        </nav>
        <main class="main">
            <section class="main__home">
                <p class="home__fact1">Selon la Banque mondiale, un habitant produit en moyenne 0,74 kg de déchets au
                    quotidien dans le monde, bien qu'il y ait de fortes disparités selon le niveau de vie. Plus ce
                    dernier est élevé, plus la quantité de déchets l'est aussi.
                    Brundtland</p>
                <p class="home__fact2">En 2020, en France, 310 millions de tonnes de déchets ont été produites. Dont
                    33,7 millions de tonnes par les ménages soit plus d'un quart du total.</p>
                <a class="home__button" href="game.html">Trier des déchets</a>
                <section class="home__guide">
                    <img class="guide__controller" src="./styles/img/controller.svg"
                        alt="espace pour sauter, gauche et droite pour se déplacer">
                    <div class="guide__bins">
                        <img class="bins__yellow" src="./styles/img/guide_yellow.png"
                            alt="Explication du tri: poubelle jaune">
                        <img class="bins__compost" src="./styles/img/guide_compost.png"
                            alt="Explication du tri: composteur">
                        <img class="bins__glass" src="./styles/img/guide_glass.png"
                            alt="Explication du tri: benne à verre">
                        <img class="bins__blue" src="./styles/img/guide_blue.png"
                            alt="Explication du tri: poubelle bleue">
                    </div>
                    <h2>Scoreboard</h2>
                    <?php include_once('viewScore.php') ?>
                    <span id="about"></span>
                </section>
                <p class="home__lore">Bienvenue à Ebolis, cette ville autrefois prospère accueillait les plus grands de
                    ce monde. Mais un jour, un homme nommé Nergal arriva avec comme objectif de répandre le chaos dans
                    cette ville. Pour ce faire, il s’y prit d’une manière très particulière, il fit en sorte que les
                    déchets ne puissent plus être ramassés. Cette méthode qui peut faire sourire au premier abord s'est
                    avérée redoutablement efficace. En très peu de temps, la ville fut ensevelie sous des montagnes de
                    déchets. Isha qui règne sur cette cité envoie son plus fidèle soldat Super Éboueur.</p>
            </section>
            <section class="main__about">
                <section class="about__project">
                    <h3 class="about__title">Le projet</h3>
                    <p class="about__paragraph">Nous devions réaliser un projet portant sur le développement durable à
                        la fin de notre second semestre de Licence Informatique. Une sélection de thème nous a été
                        proposé nous avons sélectionné : "Créer un jeu en relation avec le développement durable, avec
                        une gestion des meilleurs scores et une interface graphique en utilisant le JavaScript."</p>
                    <p class="about__paragraph">Notre site doit contenir des éléments de sensibilisation au tri
                        domestique, ainsi que notre jeu. Concernant le jeu, c'est un platformer 2d
                        où les plateformes ainsi que les déchets sont générés procéduralement. Le joueur contrôlera un
                        personnage au contrôle basique. Le gameplay sera centré sur le tri des déchets avec différents
                        types de déchets tous associés à une poubelle, le joueur devra récupérer les déchets et les
                        transportés jusqu’à la poubelle correspondante. Les déchets, ainsi que les poubelles, auront des
                        apparences différentes afin de pouvoir correctement les distinguer. L'apparence du joueur, des
                        plateformes ainsi que le background seront travailler pour correspondre à l'histoire du jeu.</p>
                    <p class="about__paragraph">Garbage Day est un jeu de plateformes 2D jouable en navigateur, autour
                        du thème du tri sélectif. Le
                        joueur contrôle un personnage pouvant courir et sauter, évoluant dans un tableau fixe où se
                        trouvent des plateformes, divers déchets, et des poubelles colorées. Le but du joueur est de
                        collecter ces déchets et de les amener dans la poubelle correspondante, pour augmenter leur
                        score et obtenir le meilleur score possible.</p>
                </section>
                <section class="about__team">
                    <h3 class="team__title">L'équipe</h3>
                    <ul class="team__teamates">
                        <li class="teamates__teamate">Sacha Daude</li>
                        <li class="teamates__teamate">Henri Charonnet</li>
                        <li class="teamates__teamate">Rémy Caduc</li>
                        <li class="teamates__teamate">Grégoire Brosset</li>
                        <li class="teamates__teamate">Chihab Chafi</li>
                    </ul>
                    <p class="about__paragraph">Une équipe composé d'étudiants en première année de licence
                        informatique à l’université de La Rochelle.</p>
                </section>
                <section class="about__utils">
                    <h3 class="utils__title">Les outils</h3>
                    <ul class="utils__ressources">
                        <li class="ressources__ressource">JavaScript vanilla</li>
                        <li class="ressources__ressource">HTML & CSS</li>
                        <li class="ressources__ressource">PHP et mySQL</li>
                        <li class="ressources__ressources">SVG</li>
                    </ul>
                </section>
            </section>
        </main>
        <footer>
            <section class="footer__contact">
                <a class="contact__waste"
                    href="https://www.larochelle.fr/vie-quotidienne/habitat/proprete-urbaine/dechets">Gestion des
                    déchets de La Rochelle</a>
                <a class="contact__faculty" href="https://www.univ-larochelle.fr/">Université de la Rochelle</a>
            </section>
        </footer>
    </body>

</html>