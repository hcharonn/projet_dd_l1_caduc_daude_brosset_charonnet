Bienvenue à Ebolis, cette ville autrefois prospère accueillait les plus grands de ce monde. Mais un jour, un homme nommé Nergal arriva avec comme objectif de répandre le chaos dans cette ville. Pour ce faire, il s’y prit d’une manière très particulière, il fit en sorte que les déchets ne puissent plus être ramassés. Cette méthode qui peut faire sourire au premier abord s'est avérée redoutablement efficace. En très peu de temps, la ville fut ensevelie sous des montagnes de déchets. Isha qui règne sur cette cité envoie son plus fidèle soldat Super Éboueur.

Pour jouer : Rendez-vous sur devduv.sachadaude.fr, il suffit (en attendant que le site soit mis en ligne) de télécharger le projet et d’ouvrir dans un navigateur la page index.html qui se trouve à la racine du projet. Les contrôles sont très basique : 
[espace] pour sauter
[right arrow] aller à droite 
[left arrow] aller à gauche 

Pensez à bien rentrer votre pseudo afin que le jeu puisse tourner correctement, et surtout pour battre vos amis sur votre nouveau jeu préféré.
